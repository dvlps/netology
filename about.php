<?php
error_reporting(E_ALL);
ini_set('display_errors', 1)
?>

<?php
$name = 'Alexander';
$age = '27';
$email = 'hrwebstudio@gmail.com';
$city  = 'Odessa';
$about = 'I am a PM in a small web development company.';
?>

<!DOCTYPE>
<html>
    <head>
        <title>About me</title>
        <style>
            body {
                font-family: sans-serif;  
            }
            
            dl {
                display: table-row;
            }
            
            dt, dd {
                display: table-cell;
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h1>Personal page of Alexander</h1>
        <dl>
            <dt>Name</dt>
            <dd><?= $name ?></dd>
        </dl>
        <dl>
            <dt>Age</dt>
            <dd><?= $age ?></dd>
        </dl>
        <dl>
            <dt>E-mail</dt>
            <dd><a href="mailto:<?= $email ?>"><?= $email ?></a></dd>
        </dl>
        <dl>
            <dt>City</dt>
            <dd><?= $city ?></dd>
        </dl>
        <dl>
            <dt>About me</dt>
            <dd><?= $about ?></dd>
        </dl>
    </body>
</html>