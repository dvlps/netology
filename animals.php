<?php
error_reporting(E_ALL);
ini_set('display_errors', 1)
?>

<?php

$continents = [
        'Eurasia' => [
            'Ursus arctos', 'Canis lupus', 'Sciurus'
        ],
        'North America' => [
            'Myadina Gray', 'Alligator mississippiensis', 'Mustela nivalis'
        ],
        'South America' => [
            'Eunectes murinus', 'Caiman crocodilus', 'Lama glama'
        ],    
        'Africa' => [
            'Panthera leo', 'Equus zebra', 'Giraffa camelopardalis'
        ],    
        'Australia' => [
            'Ornithorhynchus', 'Phascolarctos cinereus', 'Sarcophilus laniarius'
        ],    
        'Antarctica' => [
            'Pygoscelis antarctica', 'Ursus maritimus', 'Odobenus rosmarus'
        ],        
    ];

    foreach ($continents as $name => $animals) {
        foreach ($animals as $animal) {
            $words = explode(" ", $animal);
            if (count($words) == 2) {
                $firstWords[$name][] = $words[0];
                $secondWords[$name][] = $words[1];
            }
        }
    }

    foreach ($firstWords as $continent => $words) {

        echo "<h2>$continent</h2>";
        shuffle($secondWords[$continent]);

        foreach ($words as $key => $word) {
            $fantasticAnimal = $word." ".$secondWords[$continent][$key];
            echo "<span>$fantasticAnimal</span>";
            if ($key != (count($words) -1)) {
                echo ", ";
            }
        }
    }

?>